import sys
import os
import re
import subprocess


cmd = ' '.join(sys.argv[1:-2])
cmd = cmd.replace('LABOS', sys.argv[-2])
print('Executing:', cmd)
i_extensions = ['a','ul', 'in']
o_extensions = ['b','iz', 'out']

print('--- BEGIN TESTS ---')
for test_dir in sorted(os.listdir(sys.argv[-1])):
    test_dir_path = os.path.join(sys.argv[-1], test_dir)
    try:
        i = [os.path.join(test_dir_path, x) for x in os.listdir(test_dir_path) if re.match('[^.]*\.({})'.format('|'.join(i_extensions)), x)][0]
        o = [os.path.join(test_dir_path, x) for x in os.listdir(test_dir_path) if re.match('[^.]*\.({})'.format('|'.join(o_extensions)), x)][0]
    except Exception as e:
        print('Couldn\'t find test in', test_dir_path)
        continue
    print(test_dir, end='')
    with open(i, 'r') as a, open(o, 'r') as b:
        p = subprocess.Popen(cmd, stdin=a, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        try:
            out, err = p.communicate(timeout=10)
        except subprocess.TimeoutExpired:
            print(' EXCEEDED TIME LIMIT')
            continue
        expected = b.read().strip()
        p.wait()
    print(' OK' if out.decode('utf-8').strip() == expected else ' WRONG')
print('--- END TESTS ---')
