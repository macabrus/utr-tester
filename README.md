# Zovi ovak:

python3 <checker.py> <kalup_za_poziv> <put_do_labosa> <put_do_testova>

## Primjer kalupa za poziv ('LABOS' je obavezna ključna riječ):
    - java LABOS
    - ./LABOS
    - python LABOS
    - bash LABOS
    - python3 LABOS

## Dakle, ako radim u javi, ovak se zove:

```
python3 checker.py java LABOS put/do/kompajlane/java/klase put/do/testova
```

## ...A ako radim u C++ onda se ovak zove \*.exe

```
python3 checker.py LABOS put/do/exe/izvršne/datoteke put/do/testova
```

